package com.algorithm.util;

import java.util.ArrayList;
import java.util.List;

import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.IgniteManager;
import com.graphsee.v3.core.ServerConfig;
import com.graphsee.v3.server.GraphImportManager;
import com.graphsee.v3.util.OSUtil;

public class GraphStartUtil {

	public static String startGraph(String fileName) {
		String localIp = OSUtil.getAddress();
		ServerConfig serverConfig = new ServerConfig();
		serverConfig.dPort = 44500;
		serverConfig.dRange = 20;
		serverConfig.cPort = 44600;
		serverConfig.cRange = 20;
		serverConfig.affinity = 0;
		List<String> ip = new ArrayList<>();
		ip.add(localIp);
		serverConfig.clusterAddress = ip;
		serverConfig.localAddress = localIp;
		IgniteManager.Instance.init(serverConfig);
		IgniteManager.Instance.load();
		GraphManager.Instance.init();
		GraphManager.Instance.load();
		String graphName = "";
		try {
			GraphImportManager.Instance.loadFromCsv(fileName);
			graphName = GraphManager.Instance.getGraphs().iterator().next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return graphName;
	}
	
}
