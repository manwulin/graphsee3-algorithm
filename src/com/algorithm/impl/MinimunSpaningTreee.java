package com.algorithm.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsGraph;

/**
 * @author cl04
 * @date 2018年4月26日
 */
public class MinimunSpaningTreee {
	private GsGraph graph;
	private List<Long> result;
	private String weightProperty;
	private Map<Long, Long> edgeAndItsWeight;
	private Set<Long> visitedVertexes;

	/**
	 * 
	 */
	public MinimunSpaningTreee(String graphName, String weightProperty) {
		graph = GraphManager.Instance.getGraph(graphName);
		this.weightProperty = weightProperty;
		init();
	}

	/**
	 * 
	 */
	private void init() {
		sorted(edgeAndItsWeight);

	}

	public void get() {
		CycleDetection cycleDectection = new CycleDetection(graph);
		Long edgeId = getMinEdgeId(edgeAndItsWeight, visitedVertexes);
		cycleDectection.dect(visitedVertexes);
		if (cycleDectection.getCycles().size() == 0)
			result.add(edgeId);
	}

}
