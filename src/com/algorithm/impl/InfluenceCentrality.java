package com.algorithm.impl;

import com.algorithm.util.GraphStartUtil;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.DegreeValue;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.core.run.GsGraphVertexComputer;
import com.graphsee.v3.util.GsGraphUtil;

public class InfluenceCentrality implements GsGraphVertexComputer<Object> {
	
	private int count;
	private EdgeCondition ec;
	private GsEdgeDirection ed;
	private boolean degreeTag;
	private boolean inTag;
	private boolean outTag;	

	public InfluenceCentrality(EdgeCondition ec, GsEdgeDirection ed, boolean degreeTag, boolean inTag,
			boolean outTag) {
		this.ec = ec;
		this.ed = ed;
		this.degreeTag = degreeTag;
		this.inTag = inTag;
		this.outTag = outTag;
	}

	public Object computer(GsGraph graph, long elementId) {
		double value = 0;
		DegreeValue degreeValue = GsGraphUtil.getDegreeByElementId(graph, elementId, ec, ed, degreeTag, inTag, outTag, false);
		if (degreeValue.getInDegree() == 0 && degreeValue.getOutDegree() == 0 && degreeValue.getUndirectDegree() == 0) {
			value = degreeValue.getDegree() / (count * 1.0);
		} else {
			value = degreeValue.getOutDegree() / (count * 1.0);
		}
		return null;
	}
	
	public void getStart(String graphName) {
		
		GsGraph graph = GraphManager.Instance.getGraph(graphName);
		
		count = GsGraphUtil.getVertexCount(graph);

		graph.graphVertexCompute(this, false);
		
	}
	
	public static void main(String[] args) {
		
		String graphName = GraphStartUtil.startGraph("example/data/DegreeCentrality/ex2/graph.json");

		EdgeCondition ec = new EdgeCondition(null, null, null);
		GsEdgeDirection ed = GsEdgeDirection.Any;
		
		InfluenceCentrality centrality = new InfluenceCentrality(ec, ed, true, true, true);
		
		centrality.getStart(graphName);
		
	}
	
}
