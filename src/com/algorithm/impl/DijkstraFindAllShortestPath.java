package com.algorithm.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache.Entry;

import com.algorithm.impl.Graph.Edge;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdge;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.element.GsProperty;
import com.graphsee.v3.core.entry.EdgeKey;
import com.graphsee.v3.core.entry.EdgeValue;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.util.GsGraphUtil;

/**
 * @author cl04
 * @date   2018年5月7日
 */
public class DijkstraFindAllShortestPath {
	private GsGraph graph;
	Map<Long, Path> map = new HashMap<Long, Path>();
	private EdgeCondition ec = new EdgeCondition(null, null, null);
	private String weightProperty;
	
	public DijkstraFindAllShortestPath(String graphName, String weightProperty) {
		graph = GraphManager.Instance.getGraph(graphName);
		this.weightProperty = weightProperty;
	}
	
	public void compute(Long u) {
		Map<Long, Double> dist = new HashMap<Long, Double>();
		Set<Long> set = new HashSet<>();
		initDist(dist);
		
		dist.put(u, 0.0);
		
		while(set.size() < 9) {
			Long minU = minDistance(dist, set);
			
			set.add(minU);
			
			if (dist.get(minU) == Integer.MAX_VALUE)
				continue;
			Map<Long, Long> otherVertex = GsGraphUtil.getNextVertexSet(graph, minU, ec, GsEdgeDirection.Any);
			
			for(Map.Entry<Long, Long> ele : otherVertex.entrySet()) {
				Long edgeId = ele.getKey();
				Long v = ele.getValue();
				double weight = getWeight(edgeId);
				if (!set.contains(v)) {
					dist.put(v, Math.min(dist.get(u), dist.get(minU) + weight));
					Path path = map.get(v);
					if (path != null) {
						if (path.dist == dist.get(v)) {
							updatePath(minU, v, path);
							
						} else if (path.dist > dist.get(v)) {
							path.removeAllPath();
							updatePath(minU, v, path);
							path.setDist(dist.get(v));
						}
						
					} else {
						path = new Path();
						Path minUPaths = map.get(minU);
						if (minUPaths == null) {
							List<Long> list = new ArrayList<>();
							list.add(minU);
							list.add(v);
							path.addPath(list);
						} else {
							updatePath(minU, v, path);
						}
						path.setDist(dist.get(v));
						map.put(v, path);
					}
				}
			}
			
		}
		
//		double sum = 0;
//		for(Map.Entry<Integer, Path> ele : map.entrySet()) {
//			if (ele.getKey() == between)
//				continue;
//			
//			Path onePath = ele.getValue();
//			List<List<Integer>> allPaths = onePath.paths;
//			
//			double value = 0;
//			double value_u = 0;
//			for(List<Integer> list : allPaths) {
//				if (list.contains(between))
//					value_u++;
//				value++;
//			}
////			System.out.println(u + " to " + ele.getKey() + ": " + ele.getValue() + " :" + value_u / value);
//			sum += value_u / value;
//		}
//		
//		return sum;
	}
	
	/**
	 * @param edgeId
	 * @return
	 */
	private double getWeight(Long edgeId) {
		GsEdge edge = GsEdge.getEdge(graph, edgeId);
		Map<String, Set<GsProperty>> propertyMap = edge.getPropertyMap();
		Set<GsProperty> set = propertyMap.get(weightProperty);
		for(GsProperty prop : set) {
			return Double.parseDouble(prop.data.toString());
		}
		return 1;
	}

	private void updatePath(Long minU, Long v, Path path) {
		Path minUPaths = map.get(minU);
		for(List<Long> minUPath : minUPaths.paths) {
			List<Long> list = new ArrayList<>();
			list.addAll(minUPath);
			list.add(v);
			path.addPath(list);
		}
	}
	
	/**
	 * @param dist
	 */
	private void initDist(Map<Long, Integer> dist) {
		// TODO Auto-generated method stub
		
	}

	private static class Path {
		private double dist = Integer.MAX_VALUE;
		private List<List<Long>> paths;
		
		Path() {
			paths = new ArrayList<List<Long>>();
		}

		public double getDist() {
			return dist;
		}

		public void setDist(double dist) {
			this.dist = dist;
		}
		
		public void addPath(List<Long> path) {
			paths.add(path);
		}
		
		public void removeAllPath() {
			paths.clear();
		}

		@Override
		public String toString() {
			return "Path [dist=" + dist + ", paths=" + paths + "]";
		}
		
	}

}
