package com.algorithm.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.cache.Cache.Entry;

import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdge;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.element.GsProperty;
import com.graphsee.v3.core.element.GsVertex;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.entry.PrimitiveKey;
import com.graphsee.v3.core.entry.PrimitiveValueDouble;
import com.graphsee.v3.core.entry.PrimitiveValueInteger;
import com.graphsee.v3.core.entry.PrimitiveValueLong;
import com.graphsee.v3.core.entry.PrimitiveValueString;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.util.GsGraphUtil;

/**
 * @author cl04
 * @date 2018年5月7日
 */
public class CommunityDectection {
	private GsGraph graph;
	private EdgeCondition ec = new EdgeCondition(null, null, null);
	private Long totalEdges, totalVertexes;
	private String weightProperty;
	private double maxModularity, modularity;
	private Map<Long, Double> communityWeight;
	private Map<Long, Integer> nVertexesPerCommunity;
	private Long nCommunity;
	// private Map<Long, Set<Long>> neighourCommunity;
	// 通过communityNo使用sql查询除相应的点，然后获取所有点的邻居所在的社区，使用中动态构造

	private void CommunityDectection(String graphName) {
		graph = GraphManager.Instance.getGraph(graphName);
	}

	public void dectect() {
		boolean update, update2;
		update = true;
		do {
			update = runLouvainAlgorithm();
			if (nCommunity < totalVertexes) {
	
				update2 = runLouvainAlgorithm();
	
				if (update2) {
					update = true;
	
				}
			}
		} while(update);
	}

	public boolean runLouvainAlgorithm() {
		boolean update;
		Set<String> typeSet = graph.getVertexType();
		List<Long> unusedCommunity = new ArrayList<Long>();
		int nUnusedCommunity = 0;
		Long elementId = null;
		int nStableVertexes = 0;
		
		if (isFirst) :
		initCommunity(); // 初始化社区，给每个vertex添加一个社区号，社区号就是节点的id
		initNVertexesPerCommunity(); // 初始化每个社区的节点数为1

		for (Map.Entry<Long, Integer> entry : nVertexesPerCommunity.entrySet()) {
			if (entry.getValue() == 0) {
				unusedCommunity.add(entry.getKey());
				nUnusedCommunity++;
			}
		}

		do {
			
			if (isFirst):
			// 获取所有的点
			for (String typeName : typeSet) {
				int typeIndex = graph.getTypeIndex(typeName);
				String property = graph.getPrimaryProperty(typeIndex);
				PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

				if (primitiveClass == PrimitiveClass.String) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData(typeName, property,
							false);
					while (it.hasNext()) {
						PrimitiveValueString s = it.next().getValue();
						elementId = s.getElementId();
						update = computeCurrentVertexMoveToCommunity(elementId, nStableVertexes, unusedCommunity,
								nUnusedCommunity, update, true);
					}
				} else if (primitiveClass == PrimitiveClass.Long) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property,
							false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
						update = computeCurrentVertexMoveToCommunity(elementId, nStableVertexes, unusedCommunity,
								nUnusedCommunity, update, true);
					}
				} else if (primitiveClass == PrimitiveClass.Integer) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData(typeName, property,
							false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
						update = computeCurrentVertexMoveToCommunity(elementId, nStableVertexes, unusedCommunity,
								nUnusedCommunity, update, true);
					}
				} else if (primitiveClass == PrimitiveClass.Double) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData(typeName, property,
							false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
						update = computeCurrentVertexMoveToCommunity(elementId, nStableVertexes, unusedCommunity,
								nUnusedCommunity, update, true);
					}
				}

			}
			
			else {
				for(Long communityNo : communityWeight.keySet()) {
					update = computeCurrentVertexMoveToCommunity(communityNo, nStableVertexes, 
							unusedCommunity, nUnusedCommunity, update, false);
				}
			}

		} while (nStableVertexes < totalVertexes);

		nCommunity = 0l;
		Map<Long, Long> newCluster = new HashMap<Long, Long>();
		for (Map.Entry<Long, Integer> entry : nVertexesPerCommunity.entrySet()) {
			if (entry.getValue() > 0) {
				newCluster.put(entry.getKey(), nCommunity);
				double weight = communityWeight.get(entry.getKey());
				communityWeight.put(nCommunity, weight);  // 更新communityWeight, 将新的社区号存储
				nCommunity++;
			}
		}
		
		Map<Long, Integer> nVertexesPerCommunityCopy = new HashMap<Long, Integer>();
		for(Map.Entry<Long, Integer> entry : nVertexesPerCommunity.entrySet()) {
			if (entry.getValue() <= 0)
				continue;
			nVertexesPerCommunityCopy.put(newCluster.get(entry.getKey()), entry.getValue());  // 将新的社区号和点个数做映射
		}

		// set every vertex communityNo as newCluster[oldCommunityNo]

	}

	/**
	 * @param elementId
	 */
	private boolean computeCurrentVertexMoveToCommunity(Long elementId, int nStableVertexes, List<Long> unusedCommunity,
			int nUnusedCommunity, boolean update, boolean isFirst) {
		Map<Double, Long> edgeToVertex = getNeighbourCommunity(elementId, isFirst);
		Map<Long, Double> edgeWeightPerCommunity = new HashMap<Long, Double>();
		Double vertexWeight = getVertexWeight(elementId, isFirst);
		Long currCommunityNo = getCommunityNo(elementId);
		for (Map.Entry<Double, Long> entry : edgeToVertex.entrySet()) {
			Long otherVertex = entry.getValue();
			Long communityNo = getCommunityNo(otherVertex);
			if (edgeWeightPerCommunity.containsKey(communityNo)) {
				Double weight = edgeWeightPerCommunity.get(communityNo);
				weight += getWeight(elementId, otherVertex);
				edgeWeightPerCommunity.put(communityNo, weight);
			} else {
				edgeWeightPerCommunity.put(communityNo, getWeight(elementId, otherVertex));
			}
		}

		communityWeight.put(currCommunityNo, communityWeight.get(currCommunityNo) - vertexWeight);
		nVertexesPerCommunity.put(currCommunityNo, nVertexesPerCommunity.get(currCommunityNo) - 1);
		if (nVertexesPerCommunity.get(currCommunityNo) == 0) {
			unusedCommunity.add(currCommunityNo);
			nUnusedCommunity++;
		}

		Long bestCommunityNo = -1L;
		maxModularity = 0;
		for (Map.Entry<Double, Long> entry : edgeToVertex.entrySet()) {
			Long otherVertex = entry.getValue();
			Long communityNo = getCommunityNo(otherVertex);

			modularity = edgeWeightPerCommunity.get(communityNo)
					- vertexWeight * communityWeight.get(communityNo) / (2 * totalEdges);

			if (modularity > maxModularity || (modularity == maxModularity) && communityNo < bestCommunityNo) {
				maxModularity = modularity;
				bestCommunityNo = communityNo;
			}

			edgeWeightPerCommunity.put(communityNo, 0d);
		}

		if (maxModularity == 0) {
			bestCommunityNo = unusedCommunity.get(nUnusedCommunity - 1);
			nUnusedCommunity--;
		}

		communityWeight.put(bestCommunityNo, communityWeight.get(bestCommunityNo) + vertexWeight);
		nVertexesPerCommunity.put(bestCommunityNo, nVertexesPerCommunity.get(bestCommunityNo) + 1);

		if (bestCommunityNo == currCommunityNo) {
			nStableVertexes++;
		} else {
			// set elementId communityNo as bestCommunityNo
			nStableVertexes = 1;
			update = true;
		}

		return update;
	}

	/**
	 * @param elementId
	 * @param otherVertex
	 * @return
	 */
	private Double getWeight(Double edgeIdOrWeight, Map<Long, Long> edgeToVertex, boolean isFirst) {
		if (isFirst) {
			return getWeight(edgeIdOrWeight);
		}
		return edgeIdOrWeight;
	}

	/**
	 * @param elementId
	 * @param isFirst
	 * @return
	 */
	private Long getCommunityNo(Long elementId) {
		return elementId;
	}

	/**
	 * @param elementId
	 * @return
	 */
	private Double getVertexWeight(Long elementId, boolean isFirst) {
		double result = 0;
		if (isFirst) {
			
			Map<Long, Long> map = GsGraphUtil.getNextVertexSet(graph, elementId, ec, GsEdgeDirection.Any);
			for(Long v : map.values()) {
				GsEdge edge = GsEdge.getEdgeJoinProperty(graph, v);
				Map<String, Set<GsProperty>> property = edge.getPropertyMap();
				if (property.containsKey(weightProperty)) {
					Set<GsProperty> set = property.get(weightProperty);
					for(GsProperty prop : set) {
						result += (Double) prop.data;
					}
				} else {
					result += 1d;
				}
				
			}
		} else {
			result =  communityWeight.get(elementId);
		}
		
		return result;
	}

	/**
	 * @param elementId
	 * @param isFirst
	 * @return
	 */
	private Map<Double, Long> getNeighbourCommunity(Long elementId, boolean isFirst) {
		Map<Double, Long> result = new HashMap<Double, Long>();
		if (isFirst) {
			Map<Long, Long> temp = GsGraphUtil.getNextVertexSet(graph, elementId, ec, GsEdgeDirection.Any);
			for(Map.Entry<Long, Long> ele : temp.entrySet()) {
				result.put(new Double(ele.getKey()), ele.getValue());
			}
		}
		/*                                                                  
		 *  query all vertexes where communityNo = elementId
		 *  for vertex in allVertex                                         :
		 *  	get nextVertexSet
		 *  	sum edge weight for all vertex in same communityNo
		 *  	put key = totalEdgeWeight, value = otherCommunityNo to result
		 */               
		
		return null;
	}

}
