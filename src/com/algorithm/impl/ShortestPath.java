package com.algorithm.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import javax.cache.Cache.Entry;

import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.entry.PrimitiveKey;
import com.graphsee.v3.core.entry.PrimitiveValueDouble;
import com.graphsee.v3.core.entry.PrimitiveValueInteger;
import com.graphsee.v3.core.entry.PrimitiveValueLong;
import com.graphsee.v3.core.entry.PrimitiveValueString;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.util.GsGraphUtil;

/**
 * @author cl04
 * @date   2018年4月26日
 */
public class ShortestPath {
	private GsGraph graph;
	private Set<Long> visitedVertexId;
	private Map<Long, Long> distTo;
	private EdgeCondition ec = new EdgeCondition(null, null, null);
	private String weightProperty;
	private Map<Long, Path> path;
	
	public ShortestPath(String graphName, String weightProperty) {
		this.graph = GraphManager.Instance.getGraph(graphName);
		this.weightProperty = weightProperty;
		init();
	}
	
	/**
	 * 
	 */
	private void init() {
		
		Set<String> typeSet = graph.getVertexType();
		for (String typeName : typeSet) {

			int typeIndex = graph.getTypeIndex(typeName);
			String property = graph.getPrimaryProperty(typeIndex);
			PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

			if (primitiveClass == PrimitiveClass.String) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData
						(typeName, property, false);
				while (it.hasNext()) {
					PrimitiveValueString s = it.next().getValue();
					Long elementId = s.getElementId();
					System.out.println(s.getData() + " " + s.getElementId());
					distTo.put(elementId, Long.MAX_VALUE);
				}
			} else if (primitiveClass == PrimitiveClass.Long) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					distTo.put(elementId, Long.MAX_VALUE);
				}
			} else if (primitiveClass == PrimitiveClass.Integer) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData
						(typeName, property, false);
				
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					distTo.put(elementId, Long.MAX_VALUE);
				}
			} else if (primitiveClass == PrimitiveClass.Double) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData
						(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					distTo.put(elementId, Long.MAX_VALUE);
				}
			}
		}
	}

	// 从一个起始点出发，获取所有点的路径，如果经常需要获取不同点的路径，可以用此方法
	public void find(Long u) {
		if (!distTo.containsKey(u)) {
			System.out.println("图中不包含" + u + "点!");
			return;
		}
		
		distTo.put(u, 0l);
		
		while(visitedVertexId.size() != distTo.size()) {
			Long minU = minDistTo(visitedVertexId, distTo);
			
			visitedVertexId.add(minU);
			
			Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, u, ec, GsEdgeDirection.Any);
			for (long edgeId : edgeSet.keySet()) {
				long nextVertexId = edgeSet.get(edgeId);
				
				if (!visitedVertexId.contains(nextVertexId)) {
					
					Path currentPath = new Path();
					if (path.containsKey(minU))
						currentPath.getValue().addAll(path.get(minU).getValue());
					else
						currentPath.getValue().add(minU);
					currentPath.getValue().add(nextVertexId);
					if (!path.containsKey(nextVertexId)) {
						path.put(nextVertexId, currentPath);  // 存储vertexId和到它的路径
					}
					
					if (weightProperty == null) {
						distTo.put(nextVertexId, Math.min(distTo.get(nextVertexId), distTo.get(minU) + 1));
					} else {
						int weight = getEdgeWeight(weightProperty);
						distTo.put(nextVertexId, Math.min(distTo.get(nextVertexId), distTo.get(minU) + weight));
					}
				}
				
			}
		}
	}

	/**
	 * @param weightProperty2
	 * @return
	 */
	//TODO
	private int getEdgeWeight(String weightProperty2) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @param visitedVertexId2
	 * @param distTo2
	 * @return
	 */
	private Long minDistTo(Set<Long> visitedVertexId, Map<Long, Long> distTo) {
		LinkedHashMap<Long, Long> result = new LinkedHashMap<>();
		Stream<Map.Entry<Long, Long>> st = distTo.entrySet().stream();
		st.sorted(Map.Entry.<Long, Long>comparingByValue().reversed()).forEach(e -> result.put(e.getKey(), e.getValue()));
		
		for(Map.Entry<Long, Long> ele : result.entrySet()) {
			Long vertexId = ele.getKey();
			if (!visitedVertexId.contains(vertexId)) {
				return vertexId;
			}
		}
		
		return null;
	}
	
	private static class Path {
		private int length;
		private List<Long> value = new ArrayList<>();
		private List<List<Long>> allValue = new ArrayList<List<Long>>();
		
		public int getLength() {
			return length;
		}
		public void setLength(int length) {
			this.length = length;
		}
		public List<Long> getValue() {
			return value;
		}
		public void setValue(List<Long> value) {
			this.value = value;
		}
		public List<List<Long>> getAllValue() {
			return allValue;
		}
		public void setAllValue(List<List<Long>> allValue) {
			this.allValue = allValue;
		}
		
	}
	

}
