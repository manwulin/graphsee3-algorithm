package com.algorithm.impl;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache.Entry;

import com.algorithm.util.GraphStartUtil;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdge;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.element.GsVertex;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.entry.PrimitiveKey;
import com.graphsee.v3.core.entry.PrimitiveValueDouble;
import com.graphsee.v3.core.entry.PrimitiveValueInteger;
import com.graphsee.v3.core.entry.PrimitiveValueLong;
import com.graphsee.v3.core.entry.PrimitiveValueString;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.util.GsGraphUtil;
import com.graphsee.v3.util.GsonUtil;

public class Biconnected {
	private GsGraph graph;
	private Map<Long, Long> disc = new HashMap<>();
	private Map<Long, Long> low = new HashMap<>();
	private LinkedList<Long> st = new LinkedList<Long>();
	private EdgeCondition ec = new EdgeCondition(null, null, null);

	private long time = 0;

	// Count is number of biconnected components. time is
	// used to find discovery times
	int count = 0;

	// Constructor
	public Biconnected(String graphName) {
		graph = GraphManager.Instance.getGraph(graphName);
	}

	void BCCUtil(long fa, long u) {

		// Initialize discovery time and low value
		time++;
		disc.put(u, time);
		low.put(u, time);
		int children = 0;

		// Go through all vertices adjacent to this
		Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, u, ec, GsEdgeDirection.Any);

		for (long edgeId : edgeSet.keySet()) {

			long nextVertexId = edgeSet.get(edgeId);

			if (!disc.containsKey(nextVertexId)) {
				children++;

				// store the edge in stack
				st.add(edgeId);
				BCCUtil(u, nextVertexId);

				// Check if the subtree rooted with 'v' has a
				// connection to one of the ancestors of 'u'
				// Case 1 -- per Strongly Connected Components Article
				if (low.get(u) > low.get(nextVertexId))
					low.put(u, low.get(nextVertexId));

				// If u is an articulation point,
				// pop all edges from stack till u -- v
				if ((disc.get(u) == 1 && children > 1) || (disc.get(u) > 1 && low.get(nextVertexId) >= disc.get(u))) {
					System.out.println(u + " " + nextVertexId + " " + children + " " + edgeId);
					System.out.println(GsonUtil.getGson().toJson(st));
					while (st.getLast() != edgeId) {
						GsEdge gsEdge = GsEdge.getEdge(graph, st.getLast());
						GsVertex fromVertex = GsVertex.getVertex(graph, gsEdge.getFromId());
						GsVertex toVertex = GsVertex.getVertex(graph, gsEdge.getToId());
						System.out.print(fromVertex.getPrimaryKey() + "--" + toVertex.getPrimaryKey() + " ");
						st.removeLast();
					}
					GsEdge gsEdge = GsEdge.getEdge(graph, edgeId);
					GsVertex fromVertex = GsVertex.getVertex(graph, gsEdge.getFromId());
					GsVertex toVertex = GsVertex.getVertex(graph, gsEdge.getToId());
					System.out.print(fromVertex.getPrimaryKey() + "--" + toVertex.getPrimaryKey() + " ");
					st.removeLast();
					System.out.println();
					count++;
				}
			}

			// Update low value of 'u' only of 'v' is still in stack
			// (i.e. it's a back edge, not cross edge).
			// Case 2 -- per Strongly Connected Components Article
			else if (nextVertexId != fa && disc.get(nextVertexId) < low.get(u)) {
				if (low.get(u) > disc.get(nextVertexId))
					low.put(u, disc.get(nextVertexId));
				st.add(edgeId);
			}

		}
	}

	// The function to do DFS traversal. It uses BCCUtil()
	void BCC() {

		Set<String> typeSet = graph.getVertexType();

		// 获取所有的点
		for (String typeName : typeSet) {

			int typeIndex = graph.getTypeIndex(typeName);
			String property = graph.getPrimaryProperty(typeIndex);
			PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

			if (primitiveClass == PrimitiveClass.String) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData(typeName, property, false);
				while (it.hasNext()) {
					PrimitiveValueString s = it.next().getValue();
					Long elementId = s.getElementId();
					System.out.println(s.getData() + " " + s.getElementId());
					computer(elementId);
				}
			} else if (primitiveClass == PrimitiveClass.Long) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					computer(elementId);
				}
			} else if (primitiveClass == PrimitiveClass.Integer) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					computer(elementId);
				}
			} else if (primitiveClass == PrimitiveClass.Double) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					computer(elementId);
				}
			}

		}

	}

	private void computer(long elementId) {
		if (!disc.containsKey(elementId))
			BCCUtil(0, elementId);

		int j = 0;

		// If stack is not empty, pop all edges from stack
		while (st.size() > 0) {
			j = 1;
			GsEdge gsEdge = GsEdge.getEdge(graph, st.getLast());
			GsVertex fromVertex = GsVertex.getVertex(graph, gsEdge.getFromId());
			GsVertex toVertex = GsVertex.getVertex(graph, gsEdge.getToId());
			System.out.print(fromVertex.getPrimaryKey() + "--" + toVertex.getPrimaryKey() + " ");
			st.removeLast();
		}
		if (j == 1) {
			System.out.println();
			count++;
		}
	}

	public static void main(String args[]) {
		String graphName = GraphStartUtil.startGraph("example/data/biconnected/ex2/graph.json");
		Biconnected g = new Biconnected(graphName);

		g.BCC();

		System.out.println("Above are " + g.count + " biconnected components in graph");
	}

}
