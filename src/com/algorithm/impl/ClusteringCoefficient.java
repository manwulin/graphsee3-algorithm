package com.algorithm.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.algorithm.util.GraphStartUtil;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.core.run.GsGraphVertexComputer;
import com.graphsee.v3.util.GsGraphUtil;

public class ClusteringCoefficient implements GsGraphVertexComputer<Object> {

	private EdgeCondition ec;
	private GsEdgeDirection ed;
	
	public ClusteringCoefficient(EdgeCondition ec, GsEdgeDirection ed) {
		this.ec = ec;
		this.ed = ed;
	}
	
	public Object computer(GsGraph graph, long elementId) {
		// clustering_coefficient
		double clustering_coefficient = 0d;

		Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, elementId, ec, ed);

		Set<Long> vertexSet = new HashSet<>();
		edgeSet.forEach((k, v) -> {
			vertexSet.add(v);
		});

		if (vertexSet.size() > 1) {
			Set<String> connect = new HashSet<>();
			for (long nextVertexId : vertexSet) {
				Map<Long, Long> nextEdgeSet = GsGraphUtil.getNextVertexSet(graph, nextVertexId, ec, ed);
				nextEdgeSet.forEach((k, v) -> {
					if (vertexSet.contains(v)) {
						if (nextVertexId > v) {
							connect.add(v + " " + nextVertexId);
						} else {
							connect.add(nextVertexId + " " + v);
						}
					}
				});
			}
			int depth1VertexSize = vertexSet.size();
			double baseSize = (depth1VertexSize * (depth1VertexSize - 1) / 2);
			clustering_coefficient = ((double) connect.size()) / baseSize;
		}
		System.out.println(clustering_coefficient);

		return null;
		
	}

	public void getStart(String graphName) {

		GsGraph graph = GraphManager.Instance.getGraph(graphName);
		graph.graphVertexCompute(this, false);

	}

	public static void main(String[] args) {

		String graphName = GraphStartUtil.startGraph("example/data/DegreeCentrality/ex2/graph.json");

		EdgeCondition ec = new EdgeCondition(null, null, null);
		GsEdgeDirection ed = GsEdgeDirection.Any;

		ClusteringCoefficient coefficient = new ClusteringCoefficient(ec, ed);
		coefficient.getStart(graphName);

	}

}
