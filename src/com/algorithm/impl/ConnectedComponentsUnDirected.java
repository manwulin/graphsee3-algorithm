package com.algorithm.impl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.cache.Cache.Entry;

import com.csvreader.CsvWriter;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.entry.EdgeKey;
import com.graphsee.v3.core.entry.EdgeValue;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.entry.PrimitiveKey;
import com.graphsee.v3.core.entry.PrimitiveValueDouble;
import com.graphsee.v3.core.entry.PrimitiveValueInteger;
import com.graphsee.v3.core.entry.PrimitiveValueLong;
import com.graphsee.v3.core.entry.PrimitiveValueString;

/**
 * @author cl04
 * @date   2018年5月7日
 */
public class ConnectedComponentsUnDirected {
	private GsGraph graph;
	private String output;
	
	public ConnectedComponentsUnDirected(String graphName, String output) {
		graph = GraphManager.Instance.getGraph(graphName);
		this.output = output;
	}
	
	public void search() {
		CsvWriter writer = null;
		Set<Long> visited = new HashSet<Long>();
		try {
			writer = new CsvWriter(output, ',', Charset.forName("utf-8"));
			Set<String> typeSet = graph.getVertexType();

			// 获取所有的点
			int componentNo = 0;
			for (String typeName : typeSet) {

				int typeIndex = graph.getTypeIndex(typeName);
				String property = graph.getPrimaryProperty(typeIndex);
				PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

				Long elementId = null;
				if (primitiveClass == PrimitiveClass.String) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData(typeName, property, false);
					while (it.hasNext()) {
						PrimitiveValueString s = it.next().getValue();
						elementId = s.getElementId();
					}
				} else if (primitiveClass == PrimitiveClass.Long) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property, false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
					}
				} else if (primitiveClass == PrimitiveClass.Integer) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData(typeName, property, false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
					}
				} else if (primitiveClass == PrimitiveClass.Double) {
					Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData(typeName, property, false);
					while (it.hasNext()) {
						elementId = it.next().getValue().getElementId();
					}
				}
				
				if (elementId == null) {
					continue;
				}
				
				if (visited.add(elementId)) {
					componentNo++;
					writer.writeRecord(new String[]{elementId + "", componentNo + ""});
					DFS(elementId, componentNo, writer, visited);
				}
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	/**
	 * @param elementId
	 * @param componentNo
	 * @throws IOException 
	 */
	private void DFS(Long elementId, int componentNo, CsvWriter writer, Set<Long> visited) throws IOException {
		Iterator<Entry<EdgeKey, EdgeValue>> iterator = graph.findEdgeFromVertex(elementId);
		while(iterator.hasNext()) {
			Entry<EdgeKey, EdgeValue> edgeEntry = iterator.next();
			EdgeValue edgeValue = edgeEntry.getValue();
			Long otherVertexId = edgeValue.getVertexIdTo();
			if (visited.add(otherVertexId)) {
				writer.writeRecord(new String[]{elementId + "", componentNo + ""});
			}
		}
		
		iterator = graph.findEdgeToVertex(elementId);
		while(iterator.hasNext()) {
			Entry<EdgeKey, EdgeValue> edgeEntry = iterator.next();
			EdgeValue edgeValue = edgeEntry.getValue();
			Long otherVertexId = edgeValue.getVertexIdTo();
			if (visited.add(otherVertexId)) {
				writer.writeRecord(new String[]{elementId + "", componentNo + ""});
			}
		}
		
	}

}
