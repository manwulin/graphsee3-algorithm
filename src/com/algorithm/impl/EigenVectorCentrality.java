package com.algorithm.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.algorithm.util.GraphStartUtil;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.element.GsVertex;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.core.run.GsGraphVertexComputer;
import com.graphsee.v3.core.run.GsGraphVertexResultOperate;
import com.graphsee.v3.util.GsGraphUtil;
import com.graphsee.v3.util.GsonUtil;

public class EigenVectorCentrality implements GsGraphVertexComputer<Object> {

	private EdgeCondition ec;
	private GsEdgeDirection ed;
	private Long version;
	private String xCurrent;
	private String xNext;
	private String oneKey = "xNext";
	private String twoKey = "xCurrent";
	private int count;

	public EigenVectorCentrality(EdgeCondition ec, GsEdgeDirection ed) {
		this.ec = ec;
		this.ed = ed;
		xNext = oneKey;
		xCurrent = twoKey;
	}

	public Object computer(GsGraph graph, long elementId) {

		Map<Long, Long> nextVertexSet = GsGraphUtil.getNextVertexSet(graph, elementId, ec, ed);
		Set<Long> vertexSet = new HashSet<>();

		nextVertexSet.forEach((k, v) -> {
			vertexSet.add(v);
		});

		Set<String> includePropertySet = new HashSet<>();
		includePropertySet.add(xCurrent);
		GsVertex gsVertex = GsVertex.getVertexJoinProperty(graph, elementId, includePropertySet);
		double value = (double) gsVertex.getPropertyMap().get(xCurrent).iterator().next().data;

		Set<String> nextPropertySet = new HashSet<>();
		nextPropertySet.add(xNext);

		PrimitiveClass propertyClass = PrimitiveClass.Double;

		System.out.println(vertexSet);
		for (long v : vertexSet) {
			GsVertex gsVertex1 = GsVertex.getVertexJoinProperty(graph, v, nextPropertySet);
			double value1 = 0;
			if (gsVertex1.getPropertyMap() != null && gsVertex1.getPropertyMap().containsKey(xNext)) {
				value1 = (double) gsVertex1.getPropertyMap().get(xNext).iterator().next().data;
			}
			try {
				graph.addProperty(v, xNext, version, value + value1, propertyClass, "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public void getLocalElementId(String graphName) throws Exception {

		GsGraph graph = GraphManager.Instance.getGraph(graphName);

		count = GsGraphUtil.getVertexCount(graph);
		version = graph.genNewVersion();
		init(graph, xCurrent, version);

		while (true) {
			graph.graphVertexCompute(this, false);
			graph.graphVertexCompute(new GsGraphVertexComputer<Object>() {
				@Override
				public Object computer(GsGraph graph, long elementId) {
					GsVertex gsVertex = GsVertex.getVertexJoinProperty(graph, elementId);
					System.out.println(GsonUtil.getGson().toJson(gsVertex));
					return null;
				}
			}, false);
			normalize(graph, xNext);
			graph.graphVertexCompute(new GsGraphVertexComputer<Object>() {
				@Override
				public Object computer(GsGraph graph, long elementId) {
					GsVertex gsVertex = GsVertex.getVertexJoinProperty(graph, elementId);
					System.out.println(GsonUtil.getGson().toJson(gsVertex));
					return null;
				}
			}, false);
			if (getConvergence(graph, xCurrent, xNext)) {
				break;
			}
			graph.initScoreKey(xCurrent);
			if (xNext.equals(oneKey)) {
				xNext = twoKey;
				xCurrent = oneKey;
			} else {
				xNext = oneKey;
				xCurrent = twoKey;
			}
		}

	}

	private boolean getConvergence(GsGraph graph, String property, String otherProperty) {

		Set<String> includPropertySet = new HashSet<>();
		includPropertySet.add(property);
		includPropertySet.add(otherProperty);

		GsGraphVertexComputer<Double> convergence = new GsGraphVertexComputer<Double>() {

			@Override
			public Double computer(GsGraph graph, long elementId) {
				GsVertex gsVertex = GsVertex.getVertexJoinProperty(graph, elementId, includPropertySet);
				double value = 0;
				if (gsVertex.getPropertyMap() != null && gsVertex.getPropertyMap().containsKey(property)) {
					value = (double) gsVertex.getPropertyMap().get(property).iterator().next().data;
				}
				double value1 = 0;
				if (gsVertex.getPropertyMap() != null && gsVertex.getPropertyMap().containsKey(otherProperty)) {
					value1 = (double) gsVertex.getPropertyMap().get(otherProperty).iterator().next().data;
				}
				return Math.abs(value1 - value);
			}

		};

		GsGraphVertexResultOperate<Double, Double> operate = new GsGraphVertexResultOperate<Double, Double>() {

			private double value = 0;

			@Override
			public void feed(Double t) {
				value += t;
			}

			@Override
			public Double getResult() {
				return value;
			}
		};

		double sum = graph.graphVertexCompute(convergence, operate, false);

		if (sum < count * 1.0e-6) {
			return true;
		} else {
			return false;
		}

	}

	private void normalize(GsGraph graph, String property) throws Exception {

		double count = graph.selectPropertyCount(property, "sum(data * data)");

		System.out.println(count);

		count = 1.0 / Math.sqrt(count);

		System.out.println(count);

		graph.updateProperty(property, "data = data * " + count);

	}

	private void init(GsGraph graph, String propertyName, Long version) throws Exception {

		int allCount = GsGraphUtil.getVertexCount(graph);
		double data = 1 / (allCount * 1.0);
		PrimitiveClass propertyClass = PrimitiveClass.Double;
		String source = "";

		graph.graphVertexCompute(new GsGraphVertexComputer<Object>() {

			@Override
			public Object computer(GsGraph graph, long elementId) {
				try {
					graph.addProperty(elementId, propertyName, version, data, propertyClass, source);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
		}, false);

	}

	public static void main(String[] args) throws Exception {

		String graphName = GraphStartUtil.startGraph("example/data/eigenvector/ex1/graph.json");
		EdgeCondition ec = new EdgeCondition(null, null, null);
		GsEdgeDirection ed = GsEdgeDirection.Any;
		EigenVectorCentrality centrality = new EigenVectorCentrality(ec, ed);
		centrality.getLocalElementId(graphName);
	}

}
