package com.algorithm.impl;

import com.algorithm.util.GraphStartUtil;
import com.graphsee.v3.core.GraphManager;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.core.run.GsGraphVertexComputer;
import com.graphsee.v3.util.GsGraphUtil;

public class DegreeCentrality implements GsGraphVertexComputer<Object> {

	private EdgeCondition ec;
	private GsEdgeDirection ed;
	private boolean degreeTag;
	private boolean inTag;
	private boolean outTag;
	
	public DegreeCentrality(EdgeCondition ec, GsEdgeDirection ed, boolean degreeTag, boolean inTag, boolean outTag) {
		this.ec = ec;
		this.ed = ed;
		this.degreeTag = degreeTag;
		this.inTag = inTag;
		this.outTag = outTag;
	}

	public Object computer(GsGraph graph, long elementId) {
		GsGraphUtil.getDegreeByElementId(graph, elementId, ec, ed, degreeTag, inTag, outTag, false);
		return null;
	}

	public void getLocalElementId(String graphName) {

		GsGraph graph = GraphManager.Instance.getGraph(graphName);
		graph.graphVertexCompute(this, false);

	}

	public static void main(String[] args) {
		
		String graphName = GraphStartUtil.startGraph("example/data/DegreeCentrality/ex2/graph.json");

		EdgeCondition ec = new EdgeCondition(null, null, null);
		GsEdgeDirection ed = GsEdgeDirection.Any;
		
		DegreeCentrality centrality = new DegreeCentrality(ec, ed, true, true, true);
		centrality.getLocalElementId(graphName);
//		Ignite ignite = IgniteManager.Instance.getIgnite();
//		IgniteCompute compute = ignite.compute(ignite.cluster().forServers());
//		compute.broadcast(() -> {
//			try {
//			} catch (Exception e) {
//				LogUtil.log(e);
//				e.printStackTrace();
//			}
//		});

	}

}
