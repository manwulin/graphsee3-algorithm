package com.algorithm.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache.Entry;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.internal.sql.command.SqlAlterTableCommand;
import org.apache.ignite.lang.IgniteRunnable;

import com.graphsee.v3.core.IgniteManager;
import com.graphsee.v3.core.element.GsEdgeDirection;
import com.graphsee.v3.core.element.GsGraph;
import com.graphsee.v3.core.entry.PrimitiveClass;
import com.graphsee.v3.core.entry.PrimitiveKey;
import com.graphsee.v3.core.entry.PrimitiveValueDouble;
import com.graphsee.v3.core.entry.PrimitiveValueInteger;
import com.graphsee.v3.core.entry.PrimitiveValueLong;
import com.graphsee.v3.core.entry.PrimitiveValueString;
import com.graphsee.v3.core.query.EdgeCondition;
import com.graphsee.v3.util.GsGraphUtil;

/**
 * @author cl04
 * @date 2018年4月26日
 */
public class CycleDetection {
	private GsGraph graph;
	private Set<List<Long>> cycles;
	public Set<List<Long>> getCycles() {
		return cycles;
	}

	public void setCycles(Set<List<Long>> cycles) {
		this.cycles = cycles;
	}

	private IgniteCache<Long, Boolean> halted;
	private Ignite ignite;
	private IgniteCache<Long, Sequence> sequence;
	private EdgeCondition ec = new EdgeCondition(null, null, null);


	CycleDetection(GsGraph graph) {
		this.graph = graph;
		ignite = IgniteManager.Instance.getIgnite();
		halted = ignite.getOrCreateCache("halted");
		sequence = ignite.getOrCreateCache("sequence");
		init();
	}

	/**
	 * 
	 */
	private void init() {
		Set<String> typeSet = graph.getVertexType();
		for (String typeName : typeSet) {

			int typeIndex = graph.getTypeIndex(typeName);
			String property = graph.getPrimaryProperty(typeIndex);
			PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

			if (primitiveClass == PrimitiveClass.String) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData(typeName, property,
						false);
				while (it.hasNext()) {
					PrimitiveValueString s = it.next().getValue();
					Long elementId = s.getElementId();
					System.out.println(s.getData() + " " + s.getElementId());
					halted.put(elementId, false);
					Sequence obj = new Sequence();
					obj.append(elementId);
					sequence.put(elementId, obj);
				}
			} else if (primitiveClass == PrimitiveClass.Long) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property, false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					halted.put(elementId, false);
					Sequence obj = new Sequence();
					obj.append(elementId);
					sequence.put(elementId, obj);
				}
			} else if (primitiveClass == PrimitiveClass.Integer) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData(typeName, property,
						false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					halted.put(elementId, false);
					Sequence obj = new Sequence();
					obj.append(elementId);
					sequence.put(elementId, obj);
				}
			} else if (primitiveClass == PrimitiveClass.Double) {
				Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData(typeName, property,
						false);
				while (it.hasNext()) {
					Long elementId = it.next().getValue().getElementId();
					halted.put(elementId, false);
					Sequence obj = new Sequence();
					obj.append(elementId);
					sequence.put(elementId, obj);
				}
			}
		}
	}

	public void detect() {
		IgniteCompute sendCompute = ignite.compute();
		sendCompute.run(new IgniteRunnable() {

			@Override
			public void run() {
				Set<String> typeSet = graph.getVertexType();
				for (String typeName : typeSet) {

					int typeIndex = graph.getTypeIndex(typeName);
					String property = graph.getPrimaryProperty(typeIndex);
					PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

					Long elementId = getElementId(primitiveClass, typeName, property);
					sendSquence(elementId);
				}
				

			}

			private void sendSquence(Long u) {
				Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, u, ec, GsEdgeDirection.Any);
				Sequence uSequence = sequence.get(u);
				for(Long edgeId : edgeSet.keySet()) {
					Long vertexId = edgeSet.get(edgeId);
					Sequence vertexIdSequence = sequence.get(vertexId);
					vertexIdSequence.getReceive().addAll(uSequence.send);
				}
			}
		});
		
		
		IgniteCompute checkCompute = ignite.compute();
		checkCompute.run(new IgniteRunnable() {

			@Override
			public void run() {
				Set<String> typeSet = graph.getVertexType();
				for (String typeName : typeSet) {

					int typeIndex = graph.getTypeIndex(typeName);
					String property = graph.getPrimaryProperty(typeIndex);
					PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

					Long elementId = getElementId(primitiveClass, typeName, property);
					
					Sequence elementSequence = sequence.get(elementId);
					elementSequence.send.clear();
					if (elementSequence.receive.size() == 0)
						halted.put(elementId, true);
					for(List<Long> ele : elementSequence.receive) {
						if (ele.get(0) == elementId) {
							cycles.add(ele);
						} else if (ele.contains(elementId)){
							continue;
						} else {
							ele.add(elementId);
							elementSequence.send.add(ele);
						}
					}
					
					elementSequence.receive.clear();
				}
			}
			
		});
		
		
	}
	
	public void dect(Set<Long> vertexes) {
		IgniteCompute sendCompute = ignite.compute();
		sendCompute.run(new IgniteRunnable() {

			@Override
			public void run() {
				Set<String> typeSet = graph.getVertexType();
				for (String typeName : typeSet) {

					int typeIndex = graph.getTypeIndex(typeName);
					String property = graph.getPrimaryProperty(typeIndex);
					PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

					Long elementId = getElementId(primitiveClass, typeName, property);
					if (vertexes.contains(elementId)) {
						sendSquence(elementId, vertexes);
					}
				}
				

			}

			private void sendSquence(Long u) {
				Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, u, ec, GsEdgeDirection.Any);
				Sequence uSequence = sequence.get(u);
				for(Long edgeId : edgeSet.keySet()) {
					Long vertexId = edgeSet.get(edgeId);
					Sequence vertexIdSequence = sequence.get(vertexId);
					vertexIdSequence.getReceive().addAll(uSequence.send);
				}
			}
			
			private void sendSquence(Long u, Set<Long> vertexes) {
				Map<Long, Long> edgeSet = GsGraphUtil.getNextVertexSet(graph, u, ec, GsEdgeDirection.Any);
				Sequence uSequence = sequence.get(u);
				for(Long edgeId : edgeSet.keySet()) {
					Long vertexId = edgeSet.get(edgeId);
					if (vertexes.contains(vertexId)) {
						Sequence vertexIdSequence = sequence.get(vertexId);
						vertexIdSequence.getReceive().addAll(uSequence.send);
					}
				}
			}
		});
		
		
		IgniteCompute checkCompute = ignite.compute();
		checkCompute.run(new IgniteRunnable() {

			@Override
			public void run() {
				Set<String> typeSet = graph.getVertexType();
				for (String typeName : typeSet) {

					int typeIndex = graph.getTypeIndex(typeName);
					String property = graph.getPrimaryProperty(typeIndex);
					PrimitiveClass primitiveClass = graph.getPropertyClass(typeName, property);

					Long elementId = getElementId(primitiveClass, typeName, property);
					if (vertexes.contains(elementId)) {
						Sequence elementSequence = sequence.get(elementId);
						elementSequence.send.clear();
						for(List<Long> ele : elementSequence.receive) {
							if (ele.get(0) == elementId) {
								cycles.add(ele);
							} else if (ele.contains(elementId)){
								continue;
							} else {
								ele.add(elementId);
								elementSequence.send.add(ele);
							}
						}
					}
				}
			}
			
		});
		
	}
	
	public Long getElementId(PrimitiveClass primitiveClass, String typeName, String property) {
		if (primitiveClass == PrimitiveClass.String) {
			Iterator<Entry<PrimitiveKey, PrimitiveValueString>> it = graph.findStringData(typeName,
					property, true);
			while (it.hasNext()) {
				PrimitiveValueString s = it.next().getValue();
				Long elementId = s.getElementId();
				System.out.println(s.getData() + " " + s.getElementId());
	
				return elementId;
			}
		} else if (primitiveClass == PrimitiveClass.Long) {
			Iterator<Entry<PrimitiveKey, PrimitiveValueLong>> it = graph.findLongData(typeName, property,
					true);
			while (it.hasNext()) {
				Long elementId = it.next().getValue().getElementId();
				return elementId;
			}
		} else if (primitiveClass == PrimitiveClass.Integer) {
			Iterator<Entry<PrimitiveKey, PrimitiveValueInteger>> it = graph.findIntegerData(typeName,
					property, true);
			while (it.hasNext()) {
				Long elementId = it.next().getValue().getElementId();
				return elementId;
			}
		} else if (primitiveClass == PrimitiveClass.Double) {
			Iterator<Entry<PrimitiveKey, PrimitiveValueDouble>> it = graph.findDoubleData(typeName,
					property, true);
			while (it.hasNext()) {
				Long elementId = it.next().getValue().getElementId();
				return elementId;
			}
		}
		
		return null;
	}

	private static class Sequence {
		private Set<List<Long>> send = new HashSet<List<Long>>();
		private Set<List<Long>> receive = new HashSet<List<Long>>();

		public void append(Long v) {
			for (List<Long> ele : receive) {
				ele.add(v);
			}
		}

		public Set<List<Long>> getReceive() {
			return receive;
		}

		public void setReceive(Set<List<Long>> receive) {
			this.receive = receive;
		}

		public Set<List<Long>> getSend() {
			return send;
		}

		public void setSend(Set<List<Long>> send) {
			this.send = send;
		}

	}

}
/**
 * 
 */
